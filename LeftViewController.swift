
import UIKit

class LeftViewController: UIViewController {

    @IBOutlet var views: UIView!
    @IBOutlet var leftMainButtonOutlet: UIButton!
    @IBOutlet var LeftTableOutlet: UITableView!
    var arrayOfSections : [[String]] = [
        ["Home","Shop by Category","Today's Deals"],
    ["Your Orders","Your Wish List","Your Account","Gift Cards","Try Prime"],
    ["Settings      🇮🇳","Customer Service"]
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
       /* let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(changeGesture))
        gestureRecognizer.numberOfTapsRequired = 1
        views.addGestureRecognizer(gestureRecognizer)
        }*/
   /* func changeGesture()
    {
       Disappear.backSlideLeft(obj: self)
    }*/
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         Disappear.backSlideLeft(obj: self)
    }
}
    extension LeftViewController: UITableViewDataSource ,UITableViewDelegate  {
        func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            let headerView = UIView()
            headerView.backgroundColor = UIColor.init(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha:1)
            return headerView
        }
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 1
        }
        public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
        {
            let cell:LeftSlideBarTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LeftIdentifier", for: indexPath) as!LeftSlideBarTableViewCell
            cell.lblHome.text = arrayOfSections[indexPath.section][indexPath.row]
            return cell
        }
        func numberOfSections(in tableView: UITableView) -> Int {
            return arrayOfSections.count
        }
        public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
        {
            return arrayOfSections[section].count
        }
    }



