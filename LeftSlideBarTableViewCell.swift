//
//  LeftSlideBarTableViewCell.swift
//  SlideBarProject
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class LeftSlideBarTableViewCell: UITableViewCell {

    @IBOutlet var lblHome: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
