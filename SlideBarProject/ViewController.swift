//
//  ViewController.swift
//  SlideBarProject
//
//  Created by Sierra 4 on 02/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var mainButtonOutlet: UIButton!
    @IBOutlet var viewOne: UIView!
    @IBOutlet var viewTwo: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeRight = UISwipeGestureRecognizer(target: self, action:#selector(hole))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
       let swipeLeft = UISwipeGestureRecognizer(target: self, action:#selector(hole))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        viewOne.layer.cornerRadius = 3
        viewTwo.layer.cornerRadius = 3
    }
    func hole(gesture: UISwipeGestureRecognizer)
    {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
        if (swipeGesture.direction == UISwipeGestureRecognizerDirection.right)
        {
            Appear.appearSlideLeft(obj: self)
        }else if swipeGesture.direction == .left {
            Appear.appearSlideRight(obj: self)
        }
    }
    }
    @IBAction func nextButton(_ sender: UIButton) {
        Appear.appearSlideLeft(obj: self)
    }

    @IBAction func rightButton(_ sender: UIButton) {
       Appear.appearSlideRight(obj: self)
}
}
