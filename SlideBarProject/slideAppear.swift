//
//  slideAppear.swift
//  SlideBarProject
//
//  Created by Sierra 4 on 03/03/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
import UIKit

class Appear{
    
    class func appearSlideLeft(obj: UIViewController)
    {
        let viewController : LeftViewController = obj.storyboard!.instantiateViewController(withIdentifier: "LeftViewControllerIdentifier") as! LeftViewController
    obj.view.addSubview(viewController.view)
    obj.addChildViewController(viewController)
    viewController.view.layoutIfNeeded()
    viewController.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
    UIView.animate(withDuration: 0.3, animations: { () -> Void in
    viewController.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
    }, completion:nil)
}
    class func appearSlideRight(obj: UIViewController)
    {
        let viewControllerTwo : RightViewController = obj.storyboard!.instantiateViewController(withIdentifier: "RightViewControlleridentifier") as! RightViewController
        obj.view.addSubview(viewControllerTwo.view)
        obj.addChildViewController(viewControllerTwo)
        viewControllerTwo.view.layoutIfNeeded()
        viewControllerTwo.view.frame=CGRect(x: 0 + UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            viewControllerTwo.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        }, completion:nil)
    }
}
